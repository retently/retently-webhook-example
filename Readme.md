# Retently campaigner webhook request example

## System requirements

* Nodejs version v8.x


## Setup

Download sources from git repository:

    https://bitbucket.org/retently/retently-webhook-example.git
    
Navigate to ``retently-webhook-example`` folder.

Install modules:

    npm install
    
       
## Configuration

Navigate to ``retently-webhook-example/src`` folder.

Edit ``config.js`` file.

Update ``api_key`` value, with your Retently API Token https://app.retently.com/settings/api/tokens  

More details about *Webhooks guidelines* can be found here: http://help.retently.com/campaigns/general-info-about-campaigns/webhooks-guidelines   

Edit ``controller/IndexController.js`` file, edit `person` object and provide the real person credentials:

      const person = {
        'first_name': 'John',
        'last_name': 'Doe',
        'company': 'ACME',
        'email': 'johndoe@example.com',
        'tags': [
          'foo',
          'far',
        ],
      } 


## Running

Navigate to ``retently-webhook-example`` folder.

Start nodejs server:

    npm run start
    
Navigate in your browser to ``http://127.0.0.1:3000``

           
## Responses Example

No Webhook event found in Retently campaign

        {
            success: false
        }
        
Missing api key
        
        {
            message: "Missing api key.",
            code: 401,
            data: null
        }        

Unauthorized request     

        {
            message: "You are not logged in.",
            code: 401,
            data: null
        }

New person create

        {
            success: true,
            message: "Request sent successfully.",
            data: [
                {
                    success: true,
                    message: "Persons created successfully",
                    persons: {
                        johndoe@example.com: {
                            id: "11e8a82da48d5ec2514ff454",
                            status: "created"
                        }
                    }
                }
            ]
        }        
        
Customer exist      
                
        {
            success: true,
            message: "Request sent successfully.",
            data: [
                {
                    success: false,
                    event: "customer_create",
                    reason: "exists"
                }
            ]
        }        
        
Sending transactional survey

        {
            success: true,
            message: "Request sent successfully.",
            data: [
                {
                    event: "send_survey_transactional",
                    success: true,
                    data: {
                        11e8a82da48d5ec2514ff454: {
                            status: "sending",
                            to: "John Doe"
                        }
                    }
                }
            ]
        }        