const express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  bodyParser = require('body-parser'),
  {config} = require('./src/config')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

const routes = require('./src/routes')
routes(app)

app.listen(port)

console.log(config.app_name + ' app is running on port : ' + port)
