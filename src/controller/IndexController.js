const WebHookService = require('../service/WebHookService')

/**
 * Index action
 *
 * @param request
 * @param response
 */
exports.indexAction = function (request, response) {
  const person = {
    'first_name': 'John',
    'last_name': 'Doe',
    'company': 'ACME',
    'email': 'johndoe@example.com',
    'tags': [
      'foo',
      'bar',
    ],
  }

  WebHookService.send(person, function (err, data) {
    if (err) {
      response.status(data.code)
    }

    response.json(data)
  })
}
