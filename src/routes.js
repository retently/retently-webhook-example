'use strict'

module.exports = function (app) {
  const indexController = require('./controller/IndexController')

  app.route('/').get(indexController.indexAction)

}
