/**
 * Application configurations.
 */

const app_name = 'Retently transactional NPS example'
const app_version = '1.0.0'

const api_url = 'https://app.retently.com/api/campaign/webhook'
const api_key = ''

exports.config = {
  app_name,
  app_version,
  api_url,
  api_key,
}
