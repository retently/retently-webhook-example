const request = require('request')

const {config} = require('../config')

/**
 * Send request to Retently API
 *
 * @param person
 * @param callback
 */
exports.send = function (person, callback) {
  const options = {
    method: 'POST',
    url: config.api_url,
    headers: {
      'accept': 'application/json',
      'content-type': 'application/json',
      'authorization': 'api_key=' + config.api_key,
    },
    form: person,
  }

  request(options, function (error, response, body) {
    const data = JSON.parse(body)

    if (response.statusCode && response.statusCode !== 200) {
      return callback(true, data)
    }

    return callback(null, data)
  })

}
